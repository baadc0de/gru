use anyhow::Result;
use winit::{
    dpi::PhysicalSize,
    event::{ElementState, VirtualKeyCode, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

fn main() -> Result<()> {
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_title("GRU demo")
        .with_inner_size(PhysicalSize::new(1024, 768))
        .build(&event_loop)?;

    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Wait;

        match event {
            winit::event::Event::WindowEvent { window_id, event } if window_id == window.id() => {
                match event {
                    WindowEvent::Resized(_) => {}
                    WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                    WindowEvent::KeyboardInput { input, .. } => {
                        if input.state == ElementState::Released
                            && input.virtual_keycode == Some(VirtualKeyCode::Escape)
                        {
                            *control_flow = ControlFlow::Exit;
                        }
                    }
                    WindowEvent::ScaleFactorChanged {
                        scale_factor: _,
                        new_inner_size: _,
                    } => {}
                    WindowEvent::ModifiersChanged(_) => {}

                    _ => {}
                }
            }
            winit::event::Event::MainEventsCleared => {}
            winit::event::Event::RedrawRequested(_) => {}
            _ => {}
        }
    });
}
